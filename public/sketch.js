var check = 0;
var state = 0;
var position = 0;

function setup() {
  createCanvas(900, 600);
  //checkValidity("BPVVE");
  input = createInput();
  input.position(850, 65);

  buttonSubmit = createButton('Senden');
  buttonSubmit.position(input.x + input.width, 65);
  buttonSubmit.mousePressed(senden);

  buttonNext = createButton('Nächster Schritt');
  buttonNext.position(input.x + input.width, 150);
  buttonNext.mousePressed(next);

  greeting = createElement('h3', 'Bitte geben Sie eine Zeichenkette ein:');
  greeting.position(850, 20);
}

function draw() {
  if(check == 0){
    stroke(100);
    textSize(20);
    fill(255, 255, 255);

    // Zustände
    ellipse(100, 200, 50, 50);  // Zustand 1
    ellipse(300, 100, 50, 50);  // Zustand 2
    ellipse(300, 300, 50, 50);  // Zustand 3
    ellipse(500, 100, 50, 50);  // Zustand 4
    ellipse(500, 300, 50, 50);  // Zustand 5
    ellipse(700, 200, 50, 50);  // Zustand 6

    // Zustandsübergänge
    fill(0, 0, 0);
    line(10, 200, 75, 200);     // Anfangszustand --> 1
    text("B", 30, 185);
    line(125, 200, 275, 100);   // 1 --> 2
    text("T", 180, 135);
    line(125, 200, 275, 300);   // 1 --> 3
    text("P", 180, 285);
    fill(255, 255, 255);
    arc(300, 65, 35, 35, 1.5*PI / 2, 4.5 * PI / 2);   // 2 --> 2
    arc(300, 335, 35, 35, 3.5*PI / 2, 2.5 * PI / 2);  // 3 --> 3
    fill(0, 0, 0);
    text("S", 293, 30);
    text("T", 293, 380);
    line(325, 100, 475, 100);   // 2 --> 4
    text("X", 390, 80);
    line(320, 285, 480, 115);   // 4 --> 3
    text("X", 390, 180);
    line(325, 300, 475, 300);   // 3 --> 5
    text("V", 390, 330);
    line(500, 125, 500, 275);   // 5 --> 4
    text("P", 520, 200);
    line(525, 100, 675, 200);   // 4 --> 6
    text("S", 600, 135);
    line(525, 300, 675, 200);   // 5 --> 6
    text("V", 600, 285);
    line(725, 200, 790, 200);   // 6 --> Endzustand
    text("E", 745, 185);

    // Pfeile
    push();
    var angle = atan2(115 - 285, 480 - 320); //gets the angle of the line
    translate(320, 285); //translates to the destination vertex
    rotate(angle-HALF_PI); //rotates the arrow point
    triangle(-15*0.5, 15, 15*0.5, 15, 0, -15/2); 
    pop();

    push();
    var angle = atan2(200 - 200, 10 - 75); //gets the angle of the line
    translate(75, 200); //translates to the destination vertex
    rotate(angle-HALF_PI); //rotates the arrow point
    triangle(-15*0.5, 15, 15*0.5, 15, 0, -15/2); 
    pop();

    push();
    var angle = atan2(200 - 100, 125 - 275); //gets the angle of the line
    translate(275, 100); //translates to the destination vertex
    rotate(angle-HALF_PI); //rotates the arrow point
    triangle(-15*0.5, 15, 15*0.5, 15, 0, -15/2); 
    pop();

    push();
    var angle = atan2(200 - 300, 125 - 275); //gets the angle of the line
    translate(275, 300); //translates to the destination vertex
    rotate(angle-HALF_PI); //rotates the arrow point
    triangle(-15*0.5, 15, 15*0.5, 15, 0, -15/2); 
    pop();

    push();
    var angle = atan2(100 - 100, 325 - 475); //gets the angle of the line
    translate(475, 300); //translates to the destination vertex
    rotate(angle-HALF_PI); //rotates the arrow point
    triangle(-15*0.5, 15, 15*0.5, 15, 0, -15/2); 
    pop();

    push();
    var angle = atan2(100 - 100, 325 - 475); //gets the angle of the line
    translate(475, 100); //translates to the destination vertex
    rotate(angle-HALF_PI); //rotates the arrow point
    triangle(-15*0.5, 15, 15*0.5, 15, 0, -15/2); 
    pop();

    push();
    var angle = atan2(100 - 200, 525 - 675); //gets the angle of the line
    translate(675, 200); //translates to the destination vertex
    rotate(angle-HALF_PI); //rotates the arrow point
    triangle(-15*0.5, 15, 15*0.5, 15, 0, -15/2); 
    pop();

    push();
    var angle = atan2(300 - 200, 525 - 675); //gets the angle of the line
    translate(675, 200); //translates to the destination vertex
    rotate(angle-HALF_PI); //rotates the arrow point
    triangle(-15*0.5, 15, 15*0.5, 15, 0, -15/2); 
    pop();

    line(500, 125, 500, 275);
    push();
    var angle = atan2(275 - 125, 500 - 500); //gets the angle of the line
    translate(500, 125); //translates to the destination vertex
    rotate(angle-HALF_PI); //rotates the arrow point
    triangle(-15*0.5, 15, 15*0.5, 15, 0, -15/2); 
    pop();

    push();
    var angle = atan2(4.5 * PI / 2, 1.5 * PI / 2); //gets the angle of the line
    translate(312, 325); //translates to the destination vertex
    rotate(angle-HALF_PI); //rotates the arrow point
    triangle(-15*0.5, 15, 15*0.5, 15, 0, -15/2); 
    pop();

    push();
    var angle = atan2(-4.5 * PI / 2, 1.5 * PI / 2); //gets the angle of the line
    translate(312, 75); //translates to the destination vertex
    rotate(angle-HALF_PI); //rotates the arrow point
    triangle(-15*0.5, 15, 15*0.5, 15, 0, -15/2); 
    pop();

    push();
    var angle = atan2(200 - 200, 725 - 790); //gets the angle of the line
    translate(790, 200); //translates to the destination vertex
    rotate(angle-HALF_PI); //rotates the arrow point
    triangle(-15*0.5, 15, 15*0.5, 15, 0, -15/2); 
    pop();

    checkValidity(input.value());
  }
  check = 1;
}

async function checkValidity(input){
  fill(255, 255, 255);
  rect(90, 424, 700, 40);
  fill(0, 0, 0);
  await sleep(1000);
  if(input[0] == "B"){
    markState(1);
    state1(input, 1);
  } else text("Die Zeichenkette " + input + " ist ungültig!", 100, 450);
}
async function state1(input, pos){
  await sleep(1000);
  if(input[pos] == "T"){
    markState(2);
    state2(input, pos+1);
  } else if(input[pos] == "P"){
    markState(3);
    state3(input, pos+1);
  } else text("Die Zeichenkette " + input + " ist ungültig!", 100, 450);
}
async function state2(input, pos){
  await sleep(1000);
  if(input[pos] == "S"){
    markState(2);
    state2(input, pos+1);
  } else if(input[pos] == "X"){
    markState(4);
    state4(input, pos+1);
  } else text("Die Zeichenkette " + input + " ist ungültig!", 100, 450);
}
async function state3(input, pos){
  await sleep(1000);
  if(input[pos] == "T"){
    markState(3);
    state3(input, pos+1);
  } else if(input[pos] == "V"){
    markState(5);
    state5(input, pos+1);
  } else text("Die Zeichenkette " + input + " ist ungültig!", 100, 450);
}
async function state4(input, pos){
  await sleep(1000);
  if(input[pos] == "X"){
    markState(3);
    state3(input, pos+1);
  } else if(input[pos] == "S"){
    markState(6);
    state6(input, pos+1);
  } else text("Die Zeichenkette " + input + " ist ungültig!", 100, 450);
}
async function state5(input, pos){
  await sleep(1000);
  if(input[pos] == "P"){
    markState(4);
    state4(input, pos+1);
  } else if(input[pos] == "V"){
    markState(6);
    state6(input, pos+1);
  } else text("Die Zeichenkette " + input + " ist ungültig!", 100, 450);
}
async function state6(input, pos){
  await sleep(1000);
  if(input[pos] == "E"){
    markState(-1);
    fill(0, 0, 0);
    text("Die Zeichenkette " + input + " ist gültig!", 100, 450);
  } else text("Die Zeichenkette " + input + " ist ungültig!", 100, 450);
}
function markState(state){
  // Setze alle Zustände zurück
  fill(255, 255, 255);
  ellipse(100, 200, 50, 50);  // Zustand 1
  ellipse(300, 100, 50, 50);  // Zustand 2
  ellipse(300, 300, 50, 50);  // Zustand 3
  ellipse(500, 100, 50, 50);  // Zustand 4
  ellipse(500, 300, 50, 50);  // Zustand 5
  ellipse(700, 200, 50, 50);  // Zustand 6

  if(state == 1){
    fill(0,0,0);
    ellipse(100, 200, 50, 50);  // Zustand 1
  } else if(state == 2){
    fill(0,0,0);
    ellipse(300, 100, 50, 50);  // Zustand 2
  } else if(state == 3){
    fill(0,0,0);
    ellipse(300, 300, 50, 50);  // Zustand 3
  } else if(state == 4){
    fill(0,0,0);
    ellipse(500, 100, 50, 50);  // Zustand 4
  } else if(state == 5){
    fill(0,0,0);
    ellipse(500, 300, 50, 50);  // Zustand 5
  } else if(state == 6){
    fill(0,0,0);
    ellipse(700, 200, 50, 50);  // Zustand 6
  }
}
function sleep(millisecondsDuration)
{
  return new Promise((resolve) => {
    setTimeout(resolve, millisecondsDuration);
  })
}
function senden(){
  check = 0;
}
function next(){
  if(state == 0 && input.value()[position] == "B"){
    state = 1;
    markState(1);
    position++;
  } else if(state == 1 && input.value()[position] == "T"){
    state = 2;
    markState(2);
    position++;
  } else if(state == 1 && input.value()[position] == "P"){
    state = 3;
    markState(3);
    position++;
  } else if(state == 2 && input.value()[position] == "S"){
    state = 2;
    markState(2);
    position++;
  } else if(state == 2 && input.value()[position] == "X"){
    state = 4;
    markState(4);
    position++;
  } else if(state == 3 && input.value()[position] == "T"){
    state = 3;
    markState(3);
    position++;
  } else if(state == 3 && input.value()[position] == "V"){
    state = 5;
    markState(5);
    position++;
  } else if(state == 4 && input.value()[position] == "x"){
    state = 3;
    markState(3);
    position++;
  } else if(state == 4 && input.value()[position] == "S"){
    state = 6;
    markState(6);
    position++;
  } else if(state == 5 && input.value()[position] == "P"){
    state = 4;
    markState(4);
    position++;
  } else if(state == 5 && input.value()[position] == "V"){
    state = 6;
    markState(6);
    position++;
  } else if(state == 6 && input.value()[position] == "E"){
    state = 7;
    markState(7);
    text("Die Zeichenkette " + input.value() + " ist gültig!", 100, 450);
    position++;
  } else{
    text("Die Zeichenkette " + input.value() + " ist ungültig!", 100, 450);
  }
}